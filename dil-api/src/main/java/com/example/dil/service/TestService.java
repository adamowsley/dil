package com.example.dil.service;

import com.example.dil.model.Test;

import java.util.List;

public interface TestService {

    /**
     *
     * @return
     */
    List<Test> findAll();

    /**
     * Finds a test that matches the specified test identifier.
     *
     * @param testId the test identifier
     * @return a list of questions
     */
    Test findByTestId(String testId);

    /**
     * Save a list of tests.
     *
     * @param tests the list of tests to save
     * @return the saved list of questions
     */
    List<Test> save(List<Test> tests);
}
