package com.example.dil.service;

public interface GeneralService {

    /**
     *
     */
    Boolean clearMongoDB();

    /**
     *
     * @return
     */
    Boolean seedQuestions();

    /**
     *
     * @return
     */
    Boolean seedTests();



}
