package com.example.dil.service.impl;

import com.example.dil.model.Test;
import com.example.dil.repository.TestRepository;
import com.example.dil.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private TestRepository testRepository;

    @Override
    public List<Test> findAll() {
        return testRepository.findAll();
    }

    @Override
    public Test findByTestId(String testId) {
        return testRepository.findByTestId(testId);
    }

    /**
     * Save a list of questions presumably containing answers.
     *
     * @param tests the list of tests to save
     * @return the saved list of tests
     */
    @Override
    public List<Test> save(List<Test> tests) {
        return testRepository.saveAll(tests);
    }
}
