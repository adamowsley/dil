package com.example.dil.service.impl;

import com.example.dil.model.Question;
import com.example.dil.model.dto.QuestionDto;
import com.example.dil.repository.QuestionRepository;
import com.example.dil.service.QuestionService;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    @Override
    public List<Question> findByTestId(String testId) {
        return questionRepository.findByTestId(testId);
    }

    @Override
    public List<Question> saveAll(QuestionDto payload) {
        List<Question> questions = payload.getQuestions();
        return questionRepository.saveAll(questions);
    }
}