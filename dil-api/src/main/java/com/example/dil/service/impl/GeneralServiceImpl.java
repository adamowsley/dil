package com.example.dil.service.impl;

import com.example.dil.model.Question;
import com.example.dil.model.Test;
import com.example.dil.repository.QuestionRepository;
import com.example.dil.repository.TestRepository;
import com.example.dil.service.GeneralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class GeneralServiceImpl implements GeneralService {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private TestRepository testRepository;

    @Override
    public Boolean clearMongoDB() {
        return false;
    }

    @Override
    public Boolean seedQuestions() {
        Date currentDate = new Date();
        String testId = "120-100";

        try {
            Question question = new Question();
            question.setTestId(testId);
            question.setCreatedDate(currentDate);
            question.setModifiedDate(currentDate);
            question.setDescription("This is number 1 question to be answered");
            question.setAnswer("default answer");
            questionRepository.save(question);

            question = new Question();
            question.setTestId(testId);
            question.setCreatedDate(currentDate);
            question.setModifiedDate(currentDate);
            question.setDescription("This is number 2 question to be answered");
            question.setAnswer("default answer");
            questionRepository.save(question);

            question = new Question();
            question.setTestId(testId);
            question.setCreatedDate(currentDate);
            question.setModifiedDate(currentDate);
            question.setDescription("This is number 3 question to be answered");
            question.setAnswer("default answer");
            questionRepository.save(question);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public Boolean seedTests() {
        Date currentDate = new Date();

        Test test = testRepository.findByTestId("120-100");
        try {
            test = new Test();
            test.setName("This is the Test Name or Title");
            test.setDescription("This test determines if equipment xyz attains its full operational capability");
            test.setCreatedDate(currentDate);
            test.setModifiedDate(currentDate);
            test.setTestId("120-100");
            testRepository.save(test);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
