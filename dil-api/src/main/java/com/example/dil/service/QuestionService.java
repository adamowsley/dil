package com.example.dil.service;

import com.example.dil.model.Question;
import com.example.dil.model.dto.QuestionDto;

import java.util.List;

/**
 *
 */
public interface QuestionService {

    /**
     * Finds all questions that match the specified test identifier.
     *
     * @param testId the test identifier
     * @return a list of questions
     */
    List<Question> findByTestId(String testId);

    /**
     * Save a list of questions presumably containing answers.
     *
     * @param payload the list of questions to save
     * @return the saved list of questions
     */
    List<Question> saveAll(QuestionDto payload);
}
