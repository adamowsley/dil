package com.example.dil.model.dto;

public class CommandDto {

    private Integer command;

    public Integer getCommand() {
        return command;
    }

    public void setCommand(Integer command) {
        this.command = command;
    }
}
