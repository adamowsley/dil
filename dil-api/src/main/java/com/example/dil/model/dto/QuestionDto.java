package com.example.dil.model.dto;

import com.example.dil.model.Question;

import java.util.List;

public class QuestionDto {

    private List<Question> questions;

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
