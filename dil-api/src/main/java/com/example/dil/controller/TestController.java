package com.example.dil.controller;

import com.example.dil.model.Test;
import com.example.dil.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${controller.prefix}/test")
@CrossOrigin(origins = "http://localhost:4200")
public class TestController {

    @Autowired
    private TestService testService;

    @GetMapping("/all")
    public List<Test> findAll() {
        return testService.findAll();
    }

    @GetMapping("/testid/{testId}")
    public Test findByTestId(@PathVariable String testId) {
        return testService.findByTestId(testId);
    }

}
