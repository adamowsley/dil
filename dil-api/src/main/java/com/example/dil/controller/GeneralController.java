package com.example.dil.controller;

import com.example.dil.model.dto.CommandDto;
import com.example.dil.service.GeneralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${controller.prefix}/general")
@CrossOrigin(origins = "http://localhost:4200")
public class GeneralController {

    @Autowired
    private GeneralService generalService;

//    @PostMapping("/seed/0")
//    public Boolean clearMongoDB() {
//        return generalService.clearMongoDB();
//    }
//
//    @PostMapping("/seed/1")
//    public Boolean seedQuestions() {
//        return generalService.seedQuestions();
//    }
//
//    @PostMapping("/seed/2")
//    public Boolean seedTests() {
//        return generalService.seedTests();
//    }

    @PostMapping("/seed")
    public Boolean seed(@RequestBody CommandDto payload) {
        Integer command = payload.getCommand();
        if (0 == command) {
            return generalService.clearMongoDB();
        } else if (1 == command) {
            return generalService.seedQuestions();
        } else if (2 == command) {
            return generalService.seedTests();
        }
        return false;
    }
}
