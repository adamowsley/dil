package com.example.dil.controller;

import com.example.dil.model.Question;
import com.example.dil.model.dto.QuestionDto;
import com.example.dil.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${controller.prefix}/question")
@CrossOrigin(origins = "http://localhost:4200")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @PostMapping("/saveall")
    public List<Question> saveAll(@RequestBody QuestionDto payload) {
        return questionService.saveAll(payload);
    }

    @GetMapping("/testid/{testId}")
    public List<Question> findByTestId(@PathVariable String testId) {
        return questionService.findByTestId(testId);
    }
}
