package com.example.dil.repository;

import com.example.dil.model.Question;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface QuestionRepository extends MongoRepository<Question, String> {

    List<Question> findByTestId(String testId);

    @Override
    <S extends Question> List<S> saveAll(Iterable<S> iterable);
}
