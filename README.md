# Disconnected Demonstration

## Introduction

This project is a demonstration of operating a web application in a completely disconnected
state after receiving its initial data load from the web application server. 

## Architecture

This project is a typical web application consisting of a front-end Angular Material framework and 
a Spring Boot back end that fronts a MongoDB instance.

The additional features that attempt to enable a web application in a disconnected state are the use of
the Progressive Web Application and IndexedDB modules for the Angular framework.

## Conclusions and Recommendations

This project has met with some inconsistencies in operation. Testing of this application after the initial 
data load from the server usually begins with a cold start of the Chrome web client followed by accessing
the server URL. In our case the server is not executing, and the expectation is that the web application
starts in the web client as if connectivity to our web application server exists.

It seems to be hit-and-miss regarding this expectation on any given day. There are many sources that
could cause these inconsistencies. They range from erroneous application coding and PWA service worker 
configuration, shifting library dependencies, and the security posture of the client test machine. 

Using an Application Shell was not explored but may have been the proper architecture to use for
this project. See [https://developers.google.com/web/fundamentals/architecture/app-shell] 
for details regarding this architecture. Instructions for creating an Angular App Shell application
are found at [https://angular.io/guide/app-shell].

Further research _could_ succinctly determine the source of my application's inconsistent operation
or my lack of understanding for the target architecture. 

# Prerequisites

### NodeJS Installation

Navigate to [https://nodejs.org/en/] and install the latest Node.js for your development environment

### Node Package Manager

After the installation of Node.js, ensure that the Node Package Manager (npm) is properly installed
by executing the following command:

`npm -version`

### Java

Please review the prerequisites for the Apache Tomcat installation before installing Java.

### Apache Tomcat

Navigate to [http://tomcat.apache.org/] and install the latest Tomcat server for your dev/test environment.
Test the installation via the Chrome web browser by accessing [http://localhost:8080]

### MongoDB

Navigate to [https://www.mongodb.com/] and install the latest MongoDB for your dev/test environment.

### Maven

Navigate to [https://maven.apache.org/install.html] and install the latest Maven for your development environment.

# Clone the Code

Clone the code using the following command, or simply download the software if you are using
some other source control management system.

`git clone https://github.com/adamowsley/dil.git`

# Build

The build produces a war file that is deployed to your Tomcat instance. I have tested this with
no other web application servers.

Use the following command in the `dil` directory to build the war file.

`mvn clean install -DskipTests`

# Deploy

Deploy the application war file found in `dil/dil-api/target` using your Tomcat application manager.
This is usually found at [http://localhost:8080/manager/html].

Your Tomcat manager password is set in plain text in the `<your tomcat location>\conf\tomcat-users.xml` file.
Add the following to setup the Tomcat admin user for the GUI manager.

`<role rolename="manager-gui"/>`  
`<user username="tomcat" password="<your password>" roles="manager-gui"/>`

# Access

Access the web application using the following URL. This assumes that your development environment is 
all local.

[http://localhost:8080/dil]

# Observations

Application behavior does not seem consistent in regards to responsiveness during a cold start
using the access URL. Some application starts would bring an HTTP 404 error and other application
starts would properly display the home page. I have not yet found an explanation for this change
in behavior.


# Testing

Once the server and client are up and running, accessing the project URL starts the PWA. Accessing
the `Tests` menu item attempts to load the tests stored in the database. If there are none, use the menu
on the right side of the client app to _seed_ the database with tests and questions.

Once you have a test listed in the display, select the view icon to the right of the test to view its
associated questions.

Changing any answer and moving away from the answer field saves the answer in the web client's local
database.

Use the `Sync` button on the menu bar to save the answers to the remote database.

Testing of the disconnected state usually involves rebooting the client machine without starting the 
web application server or remote database. If all goes well, starting Chrome and accessing the application
URL should yield the web application home page and not an HTTP 404 error response.

Navigating to the test questions should also yield the previously entered answers from the user's last 
session.

### Scenarios

1. Normal operation includes a functioning MongoDB, Tomcat, and Chrome. Expect the 
Chrome web client to access the Tomcat server to retrieve data. Expected results include no
HTTP errors visible to user and full access to test and question data contained in the 
MongoDB.

Operating the sync button sends the local question/answer information to the web application server for
permanent storage in the external (MongoDB) data. **Note:** submitting information while disconnected did
not perform as expected. It's possible that this could have been a configuration issue.

2. Disconnected operation includes a non-functioning MongoDB and Tomcat server. The user accesses the
DIL demo using the usual URL via Chrome. The *expected* results include no HTTP errors that are
visible to the user and full access to test and question data contained in the local IndexedDB
storage that were previously obtained from MongoDB.
