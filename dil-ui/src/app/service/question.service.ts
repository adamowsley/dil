import {Injectable} from '@angular/core';
import {HttpClient, HttpRequest} from "@angular/common/http";
import {LocalStorage} from "@ngx-pwa/local-storage";
import {Question} from "../model/question";
import {ServiceBase} from "./service-base";
import {LocalDatabase} from "./local-database";
import {DiffService} from "./diff.service";
import {Observable, Subject} from "rxjs";
import {mergeMap} from "rxjs/operators";
//import 'rxjs/add/operator/map';

/**
 * The QuestionService class is responsible for loading and storing questions and related answers to and from local and remote storage.
 */
@Injectable({
  providedIn: 'root'
})
export class QuestionService extends ServiceBase implements LocalDatabase<Question, string> {

  private _questionChanged = new Subject<Question[]>();

  private questions: Question[] = []

  private testId: string;

  constructor(private diffService: DiffService,
              private httpClient: HttpClient,
              private localStorage: LocalStorage) {
    super();
  }

  get questionChanged(): Observable<Question[]> {
    return this._questionChanged.asObservable();
  }

  /**
   * Sets the new questions and triggers subscribers to process the new value.
   *
   * @param questions
   */
  setQuestions(questions: Question[]) {
    this.questions = questions;
    this._questionChanged.next(questions);
  }

  public loadQuestionsFromExternalDatabase(testId: string): Observable<Question[]> {
    console.log("QUESTION URL: " + this.baseUrl + `/question/testid/${testId}`);
    return this.httpClient.get<Question[]>(this.baseUrl + `/question/testid/${testId}`);
  }

  /**
   * Although I do have a diff, this is a demo, so just save the local straight to the external database. The external system stores each question
   * as a single item as well.
   *
   * TODO use a diff function to compare the external data to the local data; we could diff on the server side and then set the returned
   * questions
   *
   * @param items
   */
  public saveItemToExternalDatabase(questionId: string, answer: string): Observable<any> {
    console.log('saveItemToExternalDatabase');
    const req = new HttpRequest('POST', this.baseUrl + `/question/saveone`, {
      'questionId': questionId,
      'answer': answer
    }, this.httpOptions);
    return this.httpClient.request(req);
  }

  public setTestId(testId: string): void {
    this.testId = testId;
  }

  public syncToExternalDatabase(): Observable<any> {
    console.log('syncToExternalDatabase');
    if (this.questions.length > 0) {
      console.log('URL: ' + this.baseUrl + `/question/saveall`);
      // sync/save to external database
      const req = new HttpRequest('POST', this.baseUrl + `/question/saveall`, {
        'questions': this.questions
      }, this.httpOptions);
      return this.httpClient.request(req);
    }
    return null;
  }

  /**
   * Implementation from LocalDatabase to load an item from local storage
   *
   * @param key
   */
  loadItemFromLocalDatabase(key: string): Observable<any> {
    console.log('loadItemFromLocalDatabase');
    return this.localStorage.getItem<Question[]>(key);
  }

  /**
   * Implementation from LocalDatabase to store an item to local storage
   *
   * @param item
   */
  saveItemToLocalDatabase(item: Question): void {
    console.log('saveItemToLocalDatabase');
    this.localStorage.setItem(item.id, item).subscribe((next) => {
    });
  }

  /**
   * Implementation from LocalDatabase to store all items as an array to local storage. The default key for this
   * array is 'questions'.
   *
   * @param items
   */
  saveItemsToLocalStorage(key: string, items: Question[]): void {
    console.log('saveItemsToLocalDatabase');
    this.localStorage.setItem(key, items).subscribe((next) => {
    });
  }
}
