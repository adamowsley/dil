import {Injectable} from '@angular/core';
import {HttpClient, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {ServiceBase} from "./service-base";
import {LocalStorage} from "@ngx-pwa/local-storage";

/**
 * The GeneralService class provides a common service for uncategorized backend calls.
 */
@Injectable({
  providedIn: 'root'
})
export class GeneralService extends ServiceBase {

  constructor(private httpClient: HttpClient, private localStorage: LocalStorage) {
    super();
  }

  /**
   * Clears all items from local storage
   */
  public clearLocalStorage(): Observable<boolean> {
    return this.localStorage.clear();
  }

  /**
   * Seeds external storage with initial test and question entries; can also be used to clear external storage.
   *
   * Commands:
   * 0 - Clear external storage
   * 1 - Create set of initial question documents
   * 2 - Create set of initial test documents
   *
   * @param command
   */
  // public seed(command: number): Observable<Boolean> {
  //   return this.httpClient.post<Boolean>(this.baseUrl + `/general/seed/${command}`);
  // }

  public seed(command: number): Observable<any> {
    const req = new HttpRequest('POST', this.baseUrl + `/general/seed`, {
      'command': command
    }, this.httpOptions);
    return this.httpClient.request(req);
  }
}
