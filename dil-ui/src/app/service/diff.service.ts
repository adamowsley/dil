import {Injectable} from '@angular/core';

/**
 * The questions and answers provided should pass through a diff to determine if question has changed from the server.
 *
 * This class is capable of determining which questions have changed. Regarding answers, my thoughts are to favor
 * the questions and answers from the local system.
 *
 * This class was used in testing but is not included in processing in the demo; it's just here for posterity.
 *
 * An example usage to diff two objects is found below. Let there be two objects {ext0 and int0}. Map is called with
 * these instances and produces a new object that favors the information as described in the notes of the map function.
 * <code>
 *   let extO = {
 *       a: 'ext value a',
 *       b: 'ext value b',
 *       d: 'ext value d',
 *       answer: 0
 *    };
 *
 *    let intO = {
 *        a: 'int value a',
 *        b: 'int value b',
 *        c: 'int value c',
 *        d: 'hello there',
 *        answer: 11
 *    };
 *
 *    let newO = diffService.map(extO, intO);
 * </code>
 */
@Injectable({
  providedIn: 'root'
})
export class DiffService {

  constructor() {
  }

  /**
   * Map the externally stored object and the internally stored object onto a new object. This function favors the
   * values in the internal or favored object with respect to the answer field only. This function deletes favored
   * fields from returned diff when they have been removed from the master object.
   *
   * @param masterObject
   * @param favoredObject
   */
  map(masterObject, favoredObject) {
    let index: any;
    let tmpObject = {};
    let diffObject = {};

    for (index in favoredObject) {
      if (!masterObject.hasOwnProperty(index)) {
        // The favored object does not contain this field. This means that the field was
        // removed from the master object. This lets us skip the field for further processing.
        // Uncomment the following line to replace the missing field with the favored field value
        // diff[index] = favoredObject[index];
      } else if (typeof masterObject[index] != 'object' || typeof favoredObject[index] != 'object') {
        if (!(index in favoredObject) || masterObject[index] !== favoredObject[index]) {
          // Maps the favored answer field value instead of the master answer value
          if (index === 'answer') {
            diffObject[index] = favoredObject[index];
          } else {
            diffObject[index] = masterObject[index];
          }
        }
      } else if (tmpObject = this.map(masterObject[index], favoredObject[index])) {
        // We found an embedded object or array so recurse
        // This currently provides a diff of an array instead of the new favored array
        diffObject[index] = tmpObject;
      }
    }
    return diffObject;
  }
}
