import {Injectable} from '@angular/core';
import {Observable, Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Test} from "../model/test";
import {LocalStorage} from "@ngx-pwa/local-storage";
import {ServiceBase} from "./service-base";
import {LocalDatabase} from "./local-database";

/**
 * The TestService class is responsible for loading and storing questions and related answers to and from local and remote storage.
 */
@Injectable({
  providedIn: 'root'
})
export class TestService extends ServiceBase implements LocalDatabase<Test, string> {

  private _testChanged = new Subject<Test[]>();

  constructor(private httpClient: HttpClient, private localStorage: LocalStorage) {
    super();
  }

  get testChanged(): Observable<Test[]> {
    return this._testChanged.asObservable();
  }

  public getAll(): Observable<Test[]> {
    return this.httpClient.get<Test[]>(this.baseUrl + `/test/all`);
  }

  public getByTestId(testId: string): Observable<Test> {
    return this.httpClient.get<Test>(this.baseUrl + `/test/testid/${testId}`);
  }

  /**
   * The loadFromEitherDatabase method loads all test information and is called by TestComponent which then displays the
   * information to the user. The decision is made here whether to load this from the external database or
   * the local database.
   *
   * 1. Since this data is mastered externally, we want to favor loading the tests from the external database, then store these items locally.
   * 2. If we are offline, then we will attempt to load the tests from our local storage.
   * 3. If we don't have anything stored locally, then we are out of luck until we reconnect.
   */
  public loadFromEitherDatabase(): void {
    console.log('attempting to load tests from external database...');
    this.getAll().subscribe(tests => {
      if (tests && tests.length > 0) {
        this._testChanged.next(tests);
        this.saveItemsToLocalStorage("tests", tests);
      } else {
        this.loadFromLocalDatabase();
      }
    }, error => {
      console.log('encountered error loading tests from external database');
      this.loadFromLocalDatabase();
    });
  }

  loadFromLocalDatabase(): void {
    this.loadItemFromLocalDatabase('tests').subscribe((tests: Test[]) => {
      if (tests != null) {
        this._testChanged.next(tests);
      }
    });
  }

  /**
   * Implementation from LocalDatabase to load an item from local storage
   *
   * @param key
   */
  loadItemFromLocalDatabase(key: string): Observable<any> {
    return this.localStorage.getItem<Test[]>(key);
  }

  /**
   * Implementation from LocalDatabase to store an item to local storage
   *
   * @param item
   */
  saveItemToLocalDatabase(item: Test): void {
    this.localStorage.setItem(item.id, item).subscribe((next) => {
    });
  }

  /**
   * Implementation from LocalDatabase to store all items as an array to local storage. The default key for this
   * array is 'tests'.
   *
   * @param items
   */
  saveItemsToLocalStorage(key: string, items: Test[]): void {
    this.localStorage.setItem(key, items).subscribe((next) => {
      console.log('saved ' + key + ' to local database');
    });
  }
}
