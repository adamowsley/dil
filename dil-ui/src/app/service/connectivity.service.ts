import { Injectable } from '@angular/core';
import {Subject} from "rxjs";
import {ServiceBase} from "./service-base";

declare const window: any;

/**
 * The ConnectivityService class provides an observable that provides events when the client changes state from online to offline or
 * offline to online.
 */
@Injectable({
  providedIn: 'root'
})
export class ConnectivityService extends ServiceBase {

  private onLine: boolean;

  private _connectivityChanged = new Subject<boolean>();

  constructor() {
    super();

    console.log(`isOnline: ${this.getOnline()}`);

    window.addEventListener('online', () => {
      this.updateOnlineStatus();
    });
    window.addEventListener('offline', () => {
      this.updateOnlineStatus();
    });
  }

  get connectivityChanged() {
    return this._connectivityChanged.asObservable();
  }

  public getOnline() {
    this.onLine = !window.navigator.onLine;
    return this.onLine;
  }

  private updateOnlineStatus() {
    this._connectivityChanged.next(window.navigator.onLine);
  }
}
