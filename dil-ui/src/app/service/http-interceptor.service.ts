import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor() {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log(`Request URL: ${request.url}`);
    let headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');
    request = request.clone({
      headers: headers
    });
    return next.handle(request);
  }
}
