import {Observable} from "rxjs";

/**
 * The LocalDatabase interface specifies common methods for accessing local storage.
 */
export interface LocalDatabase<T, K> {

  loadItemFromLocalDatabase(key: K): Observable<any>;

  saveItemToLocalDatabase(item: T): void;

  saveItemsToLocalStorage(key: K, items: T[]): void;

}
