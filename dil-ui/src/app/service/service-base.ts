import {HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

/**
 * The ServiceBase class maintains commonly used properties related related to backend service access.
 */
export class ServiceBase {

  baseUrl: string;

  protected httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};

  protected httpHeaders: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor() {
    this.baseUrl = environment.baseUrl;
  }
}
