import {
  AfterViewInit,
  Component,
  ContentChildren,
  Directive,
  ElementRef,
  Input,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {CarouselItemDirective} from './carousel-item.directive';

import {animate, AnimationBuilder, AnimationFactory, AnimationPlayer, style} from '@angular/animations';

@Directive({
  selector: '.carousel-item'
})
export class CarouselItemElement {
}

// Carousel doesn't quite work yet
// <div style="margin: 2em" class="container">
// <carousel>
//   <ng-container *ngFor="let item of questions;">
// <ng-container *carouselItem>
// <div>
//   <span class="mat-h5">{{item.description}}</span>
// </div>
// </ng-container>
// </ng-container>
// </carousel>
// </div>
@Component({
  selector: 'carousel',
  exportAs: 'carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements AfterViewInit {

  @ContentChildren(CarouselItemDirective)
  items: QueryList<CarouselItemDirective>;

  @ViewChildren(CarouselItemElement, {read: ElementRef})
  private itemsElements: QueryList<ElementRef>;

  @ViewChild('carousel', {read: true, static: false})
  private carousel: ElementRef;

  @Input()
  timing = '250ms ease-in';

  @Input()
  showControls = true;

  private player: AnimationPlayer;

  private itemWidth: number;

  private currentSlide = 0;

  carouselWrapperStyle = {}

  next() {
    if (this.currentSlide + 1 === this.items.length) {
      return;
    }
    this.currentSlide = (this.currentSlide + 1) % this.items.length;
    const offset = this.currentSlide * this.itemWidth;
    const myAnimation: AnimationFactory = this.buildAnimation(offset);
    this.player = myAnimation.create(this.carousel.nativeElement);
    this.player.play();
  }

  private buildAnimation(offset) {
    return this.animationBuilder.build([
      animate(this.timing, style({transform: `translateX(-${offset}px)`}))
    ]);
  }

  prev() {
    if (this.currentSlide === 0) {
      return;
    }
    this.currentSlide = ((this.currentSlide - 1) + this.items.length) % this.items.length;
    const offset = this.currentSlide * this.itemWidth;
    const myAnimation: AnimationFactory = this.buildAnimation(offset);
    this.player = myAnimation.create(this.carousel.nativeElement);
    this.player.play();
  }

  constructor(private animationBuilder: AnimationBuilder) {
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit');
    console.log(this.carousel.nativeElement);
    // For some reason only here I need to add setTimeout, in my local env it's working without this.
    setTimeout(() => {
      // console.log(`itemsElements = ${this.itemsElements}`);

      this.itemWidth = this.carousel.nativeElement.getBoundingClientRect().width;
      console.log(`itemWidth = ${this.itemWidth}`);

      // this.itemWidth = this.itemsElements.first.nativeElement.getBoundingClientRect().width;
      this.carouselWrapperStyle = {
        width: `${this.itemWidth}px`
      }
    });
  }

}
