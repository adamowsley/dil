import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material";
import {Test} from "../model/test";
import {TestService} from "../service/test.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-tests',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  /**
   * The dataSource is used by the test table on the html template. Create it with
   * an array of Test is all that is needed.
   */
  dataSource: MatTableDataSource<Test>;

  /**
   * displayColumns defines the columns to display by the dataSource. The display columns must be named
   * precisely as they are in the data structure for Test.
   */
  displayColumns = ['testId', 'name', 'description', 'modifiedDate', 'actions'];

  constructor(private router: Router,
              private testService: TestService) {
    this.dataSource = new MatTableDataSource<Test>([]);
  }

  ngOnInit() {
    console.log('TestComponent : ngOnInit');
    // Subscribe to changes in the test documents; this resets the data source when a change occurs
    this.testService.testChanged.subscribe(value => {
        this.dataSource = new MatTableDataSource<Test>(value);
    });

    // Load test data from local or external database depending on our connectivity
    this.testService.loadFromEitherDatabase();
  }

  // Routers the user to the questions related to our selected test
  public displayDetails(row) {
    this.router.navigate(['/question'], {queryParams: {testId: row.testId}}).then();
  }
}
