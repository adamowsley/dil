import {Component, OnInit} from '@angular/core';
import {SwUpdate} from "@angular/service-worker";
import {GeneralService} from "./service/general.service";
import {QuestionService} from "./service/question.service";

/**
 * Maintains the menu and overall structure of the site
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Disconnected, Intermittent, & Limited';

  today: number = Date.now();

  constructor(private swUpdate: SwUpdate,
              private generalService: GeneralService,
              private questionService: QuestionService) {
  }

  ngOnInit() {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        if (confirm('A new version is available. Would you like to load it?')) {
          window.location.reload();
        }
      });
    }
  }

  clear(): void {
    this.generalService.clearLocalStorage().subscribe(response => {
      console.log(`local storage cleared: ${response}`);
    });
  }

  seed(command: number): void {
    this.generalService.seed(command).subscribe(value => {
        console.log(`seed success: ${value}`);
      },
      error => {
        console.log('no success');
      })
  }

  syncQuestions(): void {
    this.questionService.syncToExternalDatabase().subscribe(response => {
      console.log('Response');
      console.log(response);
    });
  }
}
