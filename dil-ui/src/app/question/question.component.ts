import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {MatTableDataSource} from "@angular/material";
import {QuestionService} from "../service/question.service";
import {Question} from "../model/question";
import {Test} from "../model/test";
import {TestService} from "../service/test.service";

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  /**
   * The dataSource is used by the question table on the html template. Create it with
   * an array of Question is all that is needed.
   */
  dataSource: MatTableDataSource<Question>;

  /**
   * displayColumns defines the columns to display by the dataSource. The display columns must be named
   * precisely as they are in the data structure for Question.
   */
  displayColumns = ['description', 'answer'];

  /**
   * Provides local storage of a list of questions and related answers
   */
  questions: Question[] = [];

  /**
   * Store the test so that we can display or use its information on the html template.
   */
  test: Test;

  /**
   * This component acquires a testId parameter via the activated route and loads the appropriate
   * test and related questions.
   *
   * The constructor itself only sets the data source to an empty array for initial display until
   * actual data is retrieved from the local or external database.
   *
   * @param activatedRoute contains routing information and parameters for use by this component
   * @param questionService
   * @param testService
   */
  constructor(private activatedRoute: ActivatedRoute,
              private questionService: QuestionService,
              private testService: TestService) {
    this.dataSource = new MatTableDataSource<Question>([]);
  }

  /**
   * ngOnInit is called by lifecycle events. Set up subscriptions for loading
   * test and questions. This subsequently sets actual question data on the data
   * source.
   */
  ngOnInit(): void {

    // fires when the the question data changes via QuestionService
    this.questionService.questionChanged.subscribe(value => {

      // dataSource drives the table on this component's html
      this.dataSource = new MatTableDataSource<Question>(value);

      // keep a local copy for focusOut()
      this.questions = value;
    });

    // Grab the argument(s) from the route into this component
    this.activatedRoute.queryParams.subscribe(params => {

      // Save the testId on the service for sync operations later
      this.questionService.setTestId(params.testId);

      // Load the test and its related questions
      this.testService.getByTestId(params.testId).subscribe((test: Test) => {
        if (test) {
          this.test = test;
          this.loadQuestions();
        } else {
          // check for locally stored questions
          this.testService.loadItemFromLocalDatabase(params.testId).subscribe((test: Test) => {
            this.test = test;
            this.loadQuestions();
          });
        }
      });
    });
  }

  /**
   * Save the data (answer) entered in the table to local storage. In reality this saves all questions, it's probably better to save them
   * one at a time when answering questions; convert this to single question storage later. This is just for demo purposes.
   *
   * @param row
   * @param $event
   */
  public focusOutRow(question, value): void {
    console.log('focusOutRow');
    // Find the question in our local list of questions and replace the answer with the one entered by the user
    question.answer = value;
    const index = this.questions.map(function (x) {
      return x.id;
    }).indexOf(question.id);
    this.questions[index] = question;

    // Refresh or store the list of questions containing the new answer; this is stored locally only
    this.questionService.saveItemsToLocalStorage(this.test.testId, this.questions);
  }

  /**
   * Load questions for our display. Although it would be quicker to load from the local database, I've
   * opted to attempt an external load first for simplicity. I also chose not to deploy a full diff between
   * the two sets of questions for simplicity.
   */
  loadQuestions(): void {
    console.log(`attempting external load of questions for ${this.test.testId}`);
    this.questionService.loadQuestionsFromExternalDatabase(this.test.testId).subscribe((extQuestions: Question[]) => {
      console.log(`attempting local load of questions for ${this.test.testId}`);
      this.questionService.loadItemFromLocalDatabase(this.test.testId).subscribe((intQuestions: Question[]) => {
        // A diff would be good here, but just going to do something more simple
        if (intQuestions && intQuestions.length > 0) {
          this.questionService.setQuestions(intQuestions);
        } else {
          this.questionService.setQuestions(extQuestions);
        }
      });
    });
  }
}
