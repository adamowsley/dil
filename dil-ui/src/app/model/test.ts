export interface Test {
  id: string;
  testId: string;
  name: string;
  description: string;
  createdDate: Date;
  modifiedDate: Date;
}
