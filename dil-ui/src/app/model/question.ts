export interface Question {
  id: string;
  testId: string;
  description: string;
  createdDate: Date;
  modifiedDate: Date;
  answer: string;
}
