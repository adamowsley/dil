import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

export interface User {
  name: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  formGroup: FormGroup;

  // select = new FormControl();

  selectedValue: number;

  items: User[] = [{name: 'box'}, {name: 'fastener'}, {name: 'post'}, {name: 'automobile'}, {name: 'tree'}];

  itemsFiltered: Observable<User[]>;

  itemSelected: User;

  constructor() {
    // console.log('constructor');
    this.formGroup = new FormGroup({
      select: new FormControl('')
    });
  }

  ngOnInit() {
    // console.log('ngOnInit');
    // this.selectedValue = 4;

    this.itemsFiltered = this.formGroup.get('select').valueChanges
      .pipe(
        startWith<string | User>(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(name => name ? this._filter2(name) : this.items.slice())
      );

    // this.formGroup.get('select').setValue('post');
    // console.log(this.formGroup.get('select').value);
  }

  clearSelect() {
    // console.log('clearSelect');
    this.formGroup.get('select').setValue('');
  }

  displayFn(user?: User): string | undefined {
    return user ? user.name : undefined;
  }

  private _filter(name: string): User[] {
    const filterValue = name.toLowerCase();
    return this.items.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filter2(name: string): User[] {
    const filterValue = name.toLowerCase();
    return this.items.filter(option => option.name.toLowerCase().includes(filterValue) == true);
  }


  // filterDropDowns(query) {
  //   console.log('ngOnInit');
  //   console.log(query);
  //   this.itemsFiltered = query ? this.itemsFiltered.filter(item => item.toLocaleLowerCase().includes(query.toLowerCase())) : this.items;
  // }

  // setValueOfTwo(): void {
  //   this.selectedValue = 2;
  //   this.select.setValue(2);
  // }

}
